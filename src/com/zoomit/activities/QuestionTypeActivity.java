package com.zoomit.activities;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.common.RunningGame;
import com.zoomit.common.Utils;
import com.zoomit.model.Model;

public class QuestionTypeActivity extends BaseActivity {

	String gameId;
	int questionType = 0;
	Point pos;
	String currentPlayerName = Model.instance(this).getCurrentPlayer()
			.getUsername();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_question_type);

		Intent intent = getIntent();
		gameId = intent.getExtras().getString(BaseDefs.GAME_ID);
		pos = (Point) intent.getExtras().get(BaseDefs.CLICK_POS);
		final String enemyPlayerName = intent.getExtras().getString(BaseDefs.ENEMY_PLAYER);

		final Button whoBtn = (Button) findViewById(R.id.question_btn_who);
		final Button whereBtn = (Button) findViewById(R.id.question_btn_where);
		final Button whatBtn = (Button) findViewById(R.id.question_btn_what);
		final Button confirmBtn = (Button) findViewById(R.id.confirm_question_type);
		final EditText answerET = (EditText) findViewById(R.id.answer_field_ET);

		whoBtn.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					whoBtn.setBackgroundResource(R.drawable.red_down);
					whatBtn.setBackgroundResource(R.drawable.red);
					whereBtn.setBackgroundResource(R.drawable.red);
					questionType = 1;
				}
				return false;
			}
		});
		whatBtn.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					whoBtn.setBackgroundResource(R.drawable.red);
					whatBtn.setBackgroundResource(R.drawable.red_down);
					whereBtn.setBackgroundResource(R.drawable.red);
					questionType = 2;
				}
				return false;
			}
		});
		whereBtn.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					whoBtn.setBackgroundResource(R.drawable.red);
					whatBtn.setBackgroundResource(R.drawable.red);
					whereBtn.setBackgroundResource(R.drawable.red_down);
					questionType = 3;
				}
				return false;
			}
		});
		answerET.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				answerET.setText("");
			}
		});
		confirmBtn.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					confirmBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					confirmBtn.setBackgroundResource(R.drawable.green);

					String answer = answerET.getText().toString();
					if (answer == null || answer.isEmpty()
							|| answer.equals(BaseDefs.ANSWER)) {
						Utils.displayAlertDialog(QuestionTypeActivity.this,
								R.string.msg_please_fill_answer);
						return false;
					}

					if (questionType == 0) {
						Utils.displayAlertDialog(QuestionTypeActivity.this,
								R.string.msg_choose_question_type);
						return false;
					}

					String startPoint = pos.x + ":" + pos.y;
					RunningGame currentGame = new RunningGame(gameId,
							questionType, startPoint, answer, currentPlayerName, enemyPlayerName, 0);

					saveRunningGameToDb(currentGame);

					Intent intent = new Intent(getApplicationContext(),
							MainAppActivity.class);
					startActivity(intent);
				}
				return false;
			}
		});

	}

	private void saveRunningGameToDb(RunningGame game) {
		Model.instance(this).saveToDb(game);
	}

	@Override
	public void onBackPressed() {
	}
}