package com.zoomit.activities;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.model.Model;

public class SelectImageActivity extends BaseActivity {
	private static final int SELECT_PICTURE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_select_image);

		Button fromPhoneBtn = (Button) findViewById(R.id.btn_choose_existing_image);
		fromPhoneBtn.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {

				// in onCreate or any event where your want the user to
				// select a file
				Intent intent = new Intent();
				intent.setType("image/*");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(
						Intent.createChooser(intent, "Select Picture"),
						SELECT_PICTURE);
			}
		});
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				Uri selectedImageUri = data.getData();
				InputStream iStream = null;

				byte[] inputData = null;

				try {
					iStream = getContentResolver().openInputStream(
							selectedImageUri);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				try {
					inputData = getBytes(iStream);
				} catch (IOException e) {
					e.printStackTrace();
				}

				Intent getIntent = getIntent();
				String gameId = getIntent.getExtras().getString(BaseDefs.GAME_ID);
				String enemyPlayerName = getIntent.getExtras().getString(BaseDefs.ENEMY_PLAYER);
				Model.instance(this).uploadFileToDb(inputData, gameId);
				Intent intent = new Intent(getApplicationContext(),
						ConfirmImageActivity.class);
				intent.putExtra(BaseDefs.PICTURE_URL, selectedImageUri.toString());
				intent.putExtra(BaseDefs.GAME_ID, gameId);
				intent.putExtra(BaseDefs.ENEMY_PLAYER, enemyPlayerName);
				startActivity(intent);
			}
		}
	}

	public byte[] getBytes(InputStream inputStream) throws IOException {
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		int bufferSize = BaseDefs.BUFFER_SIZE;
		byte[] buffer = new byte[bufferSize];

		int len = 0;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}
		return byteBuffer.toByteArray();
	}

	/**
	 * helper to retrieve the path of an image URI
	 */
	public String getPath(Uri uri) {
		// just some safety built in
		if (uri == null) {
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

}
