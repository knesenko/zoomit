package com.zoomit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;

public class StartGameActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_start_game);
		
		Intent intent = getIntent();
		String playerName = intent.getExtras().getString(BaseDefs.PLAYER);
		int round = intent.getExtras().getInt(BaseDefs.LEVEL);
		
		final TextView playerNameTV = (TextView) findViewById(R.id.enemy_name_TV);
		playerNameTV.setText(playerName);
		
		final TextView roundTV = (TextView) findViewById(R.id.round_num_TV);
		roundTV.setText("" + round);
		
	}
}