package com.zoomit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;

public class LoserActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_loser);

		Intent intent = getIntent();
		String answer = intent.getExtras().getString(BaseDefs.ANSWER);

		TextView answerTv = (TextView) findViewById(R.id.loser_answer_tv);
		answerTv.setText(answer);

		Button continueBtn = (Button) findViewById(R.id.loser_continue_btn);
		continueBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						MainAppActivity.class);
				startActivity(intent);
			}
		});

	}

	@Override
	public void onBackPressed() {
	}
}