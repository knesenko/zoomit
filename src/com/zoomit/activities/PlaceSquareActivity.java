package com.zoomit.activities;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.common.CustomView;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class PlaceSquareActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_place_square);

		Intent intent = getIntent();
		final String gameId = intent.getExtras().getString(BaseDefs.GAME_ID);
		final String enemyPlayerName = intent.getExtras().getString(BaseDefs.ENEMY_PLAYER);
		String imgUrl = intent.getExtras().getString(BaseDefs.PICTURE_URL);
		Uri uri = Uri.parse(imgUrl);
		ImageView imgView = (ImageView) findViewById(R.id.originalImage);
		imgView.setImageURI(uri);

		imgView.setScaleType(ScaleType.FIT_XY);

		final CustomView myView = new CustomView(this);
		FrameLayout v = (FrameLayout) findViewById(R.id.targetframe_layout);
		v.addView(myView);

		final Button confirmBtn = (Button) findViewById(R.id.confirm_square);
		confirmBtn.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					confirmBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					confirmBtn.setBackgroundResource(R.drawable.green);
					Intent intent = new Intent(getApplicationContext(),
							QuestionTypeActivity.class);
					intent.putExtra(BaseDefs.CLICK_POS, myView.pos);
					intent.putExtra(BaseDefs.GAME_ID, gameId);
					intent.putExtra(BaseDefs.ENEMY_PLAYER, enemyPlayerName);
					startActivity(intent);
				}
				return false;
			}
		});

	}
	@Override
	public void onBackPressed() {
	}

}
