package com.zoomit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.zoomit.R;
import com.zoomit.model.IsRegisterClbk;
import com.zoomit.model.Model;

public class RegisterActivity extends BaseActivity {

	private EditText mUsernameField;
	private EditText mPasswordField;
	private TextView mErrorField;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_register);

		mUsernameField = (EditText) findViewById(R.id.register_username);
		mPasswordField = (EditText) findViewById(R.id.register_password);
		mErrorField = (TextView) findViewById(R.id.error_messages);
	}

	public void register(final View v) {
		v.setEnabled(false);

		String username = mUsernameField.getText().toString();
		String password = mPasswordField.getText().toString();

		if (password.length() == 0 || username.length() == 0)
			return;

		mErrorField.setText("");

		Model.instance(this).register(username, password, new IsRegisterClbk() {

			@Override
			public void done(boolean isRegisterFinished, String msg) {

				if (isRegisterFinished) {
					Intent intent = new Intent(getApplicationContext(),
							AppStartActivity.class);
					startActivity(intent);
					return;
				}

				mErrorField.setText(msg);
				v.setEnabled(true);
			}
		});
	}

	public void showLogin(View v) {
		Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
		startActivity(intent);
		finish();
	}
}