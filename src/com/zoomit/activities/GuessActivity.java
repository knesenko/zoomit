package com.zoomit.activities;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.common.RunningGame;
import com.zoomit.common.Utils;
import com.zoomit.model.GetFileClbk;
import com.zoomit.model.GetRunningGameClbk;
import com.zoomit.model.Model;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class GuessActivity extends BaseActivity {
	RunningGame runningGame;
	Drawable image; // Remove me
	Bitmap imgBitmap;
	ImageView img;
	EditText editTxtAnswer;
	boolean ready = false;
	int zoomLevel = 1;
	int imgViewHeight;
	int imgViewWidth;
	int x;
	int y;
	int score = 100;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_guess);

		Intent intent = getIntent();
		final String gameId = intent.getExtras().getString(BaseDefs.GAME_ID);

		// get current running game from DB
		Model.instance().getRunningGame(new GetRunningGameClbk() {

			@Override
			public void done(RunningGame game) {
				runningGame = game;
				parseStartPoint(runningGame.getStartPoint());
			}
		}, gameId);

		img = (ImageView) findViewById(R.id.guess_pic_iv);

		// get game image from DB
		Model.instance(this).getFileFromDb(gameId, new GetFileClbk() {

			@Override
			public void done(byte[] file) {
				setFirstPicture(file);
			}
		});

		Button zoomBtn = (Button) findViewById(R.id.zoom_btn);
		zoomBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (zoomLevel >= 5) {
					return;
				} else {
					if (ready) {
						if (score <= 0) {
							// Surrender Game !! Do Logic HERE !
							return;
						}
						score -= 10;
						zoomLevel++;
						ZoomImage();
					}
				}
			}
		});

		editTxtAnswer = (EditText) findViewById(R.id.answer_text_field);
		Button confirmBtn = (Button) findViewById(R.id.btn_send_guess);
		confirmBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String answer = editTxtAnswer.getText().toString();

				if (answer.isEmpty()) {
					Utils.displayAlertDialog(GuessActivity.this,
							R.string.msg_please_fill_answer);
					return;
				}

				int correct = Utils.checkStringsMatch(runningGame.getAnswer(),
						answer);
				if (correct == 100) {
					Intent intent = new Intent(getApplicationContext(),
							WinnerActivity.class);
					intent.putExtra(BaseDefs.SCORE, score);
					intent.putExtra(BaseDefs.GAME_ID, gameId);
					startActivity(intent);
				} else {
					if (score <= 10) {
						Intent intent = new Intent(getApplicationContext(),
								LoserActivity.class);
						intent.putExtra(BaseDefs.ANSWER, runningGame.getAnswer());
						removeGame(gameId);
						startActivity(intent);
					}
					score -= 10;
				}

			}
		});

		Button surrenderBtn = (Button) findViewById(R.id.btn_surrender);
		surrenderBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				removeGame(gameId);

				Intent intent = new Intent(getApplicationContext(),
						MainAppActivity.class);
				startActivity(intent);
			}
		});

	}


	private void setFirstPicture(byte[] file) {
		imgBitmap = BitmapFactory.decodeByteArray(file, 0, file.length);
		imgViewHeight = imgBitmap.getHeight();
		imgViewWidth = imgBitmap.getWidth();
		ZoomImage();
		ready = true;
	}

	private void parseStartPoint(String pos) {
		String xx = pos.substring(0, pos.indexOf(':'));
		String yy = pos.substring(pos.indexOf(':') + 1, pos.length());
		x = Integer.parseInt(xx);
		y = Integer.parseInt(yy);
	}

	private void ZoomImage() {
		float right = x + (imgViewWidth * zoomLevel / 5);
		if (right > imgViewWidth) {
			right = imgViewWidth;
		}
		float left = (x - (imgViewWidth * zoomLevel / 5));
		if (left <= 0) {
			left = 0;
		}
		float up = (y - (imgViewHeight * zoomLevel / 5));
		if (up <= 0) {
			up = 0;
		}
		float down = (y + (imgViewHeight * zoomLevel / 5));
		if (down >= imgViewHeight) {
			down = imgViewHeight;
		}

		int rightLeft = (int) right - (int) left;
		int upDown = (int) down - (int) up;

		if (upDown <= 0) {
			upDown = 0;
		} else if (upDown >= imgViewHeight) {
			upDown = imgViewHeight;
		}

		if (rightLeft <= 0) {
			rightLeft = 0;
		} else if (rightLeft >= imgViewWidth) {
			rightLeft = imgViewWidth;
		}
		Bitmap croppedBmp = Bitmap.createBitmap(imgBitmap, (int) left,
				(int) up, (int) (right - left), (int) (down - up));

		img.setImageBitmap(croppedBmp);

	}

	@Override
	public void onBackPressed() {
	}
	
	private void removeGame(String gameId) {
		Model.instance().removeGame(gameId);
		Model.instance().removeRunningGame(gameId);
		Model.instance().removeFileFromDb(gameId);
	}
}
