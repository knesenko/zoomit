package com.zoomit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.model.Model;

public class WinnerActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_winner);

		Intent intent = getIntent();
		int score = intent.getExtras().getInt(BaseDefs.SCORE);
		String gameId = intent.getExtras().getString(BaseDefs.GAME_ID);

		String username = Model.instance().getCurrentPlayer().getUsername();
		// update users score
		Model.instance().updateScore(username, score);
		Model.instance().incrementLevel(gameId);
		Model.instance().updateRunningGame(gameId, username, 1);
		Model.instance().removeFileFromDb(gameId);

		intent = new Intent(getApplicationContext(), MainAppActivity.class);

		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
	}
}