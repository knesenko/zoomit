package com.zoomit.activities;

import com.zoomit.R;
import com.zoomit.model.IsloginClbk;
import com.zoomit.model.Model;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends BaseActivity {

	private EditText mUsernameField;
	private EditText mPasswordField;
	private TextView mErrorField;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_login_activity);

		mUsernameField = (EditText) findViewById(R.id.login_username);
		mPasswordField = (EditText) findViewById(R.id.login_password);
		mErrorField = (TextView) findViewById(R.id.error_messages);
	}

	public void signIn(final View v) {
		v.setEnabled(false);
		String username = mUsernameField.getText().toString();
		String password = mPasswordField.getText().toString();
		Model.instance(this).login(username, password, new IsloginClbk() {

			@Override
			public void done(boolean isLoginFinished, String msg) {

				if (isLoginFinished) {
					Intent intent = new Intent(getApplicationContext(),
							AppStartActivity.class);
					startActivity(intent);
					return;
				}

				mErrorField.setText(msg);
				v.setEnabled(true);
			}

		});
	}

	public void showRegistration(View v) {
		Intent intent = new Intent(getApplicationContext(),
				RegisterActivity.class);
		startActivity(intent);
		finish();
	}
}
