package com.zoomit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.zoomit.R;
import com.zoomit.model.Model;

public class MainActivity extends BaseActivity {
	private boolean isCurrentUserLoggedIn = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);

		Button startBtn = (Button) findViewById(R.id.start_btn_main);

		Model.instance(this).updateCurrentPlayerUser();
		isCurrentUserLoggedIn = Model.instance(this).isCurrentUserLoggedIn();

		startBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isCurrentUserLoggedIn) {
					Intent intent = new Intent(getApplicationContext(),
							AppStartActivity.class);
					MainActivity.this.startActivity(intent);
				} else {
					Intent intent = new Intent(getApplicationContext(),
							LoginActivity.class);
					startActivity(intent);
				}
			}
		});
	}
}
