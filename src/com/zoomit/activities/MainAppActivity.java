package com.zoomit.activities;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.common.Game;
import com.zoomit.common.ListAdapter;
import com.zoomit.common.RunningGame;
import com.zoomit.common.Utils;
import com.zoomit.model.GetGamesClbk;
import com.zoomit.model.GetRunningGameClbk;
import com.zoomit.model.GetScoreClbk;
import com.zoomit.model.Model;

public class MainAppActivity extends BaseActivity {

	ListAdapter adapter;
	String currentPlayer;
	RunningGame selectedGame;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main_app);

		// FILL LIST FROM DB
		final ListView listView = (ListView) findViewById(R.id.players_list);
		final List<Game> games = new ArrayList<Game>();
		currentPlayer = Model.instance(this).getCurrentPlayer().getUsername();

		final TextView scoreTV = (TextView) findViewById(R.id.score_display_tv);
		Model.instance(this).getScore(currentPlayer, new GetScoreClbk() {

			@Override
			public void done(int score) {
				scoreTV.setText("Score: " + Integer.toString(score));
			}
		});

		Model.instance(this).getAllGames(new GetGamesClbk() {

			@Override
			public void done(List<Game> list) {
				games.clear();

				// show only my games
				for (Game game : list) {
					if (game.getPlayer1().equals(currentPlayer)
							|| game.getPlayer2().equals(currentPlayer)) {
						games.add(game);
					}
				}

				adapter = new ListAdapter(games, MainAppActivity.this);
				listView.setAdapter(adapter);

				listView.setOnItemClickListener(new OnItemClickListener() {

					public void onItemClick(AdapterView adapter, View arg1,
							int position, long arg3) {
						ListAdapter ad = (ListAdapter) listView.getAdapter();
						final String game_id = ad.getItem(position).getId();
						final int level = ad.getItem(position).getLevel();
						final String enemyPlayerName = ad.getItem(position)
								.getPlayer2();
						Model.instance().getRunningGame(
								new GetRunningGameClbk() {

									@Override
									public void done(RunningGame game) {
										selectedGame = game;
										if (selectedGame == null) {
											return;
										}

										if (selectedGame.getTurn().equals(
												currentPlayer)) {
											if (selectedGame.getDidGuess() == 1) {
												Intent intent = new Intent(
														getApplicationContext(),
														SelectImageActivity.class);

												intent.putExtra(
														BaseDefs.GAME_ID,
														game_id);
												intent.putExtra(
														BaseDefs.ENEMY_PLAYER,
														enemyPlayerName);
												startActivity(intent);
											} else if (selectedGame
													.getDidGuess() == 0) {
												Intent intent = new Intent(
														getApplicationContext(),
														GuessActivity.class);

												intent.putExtra(
														BaseDefs.GAME_ID,
														game_id);
												startActivity(intent);
											}
										} else {
											Utils.displayAlertDialog(
													MainAppActivity.this,
													R.string.msg_wait_for_other_palyer);
										}
									}
								}, game_id);

					}
				});

			}
		});

		final Button newGameBtn = (Button) findViewById(R.id.new_game_btn);
		newGameBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					newGameBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					newGameBtn.setBackgroundResource(R.drawable.green);
					Intent intent = new Intent(getApplicationContext(),
							SearchUserActivity.class);
					startActivity(intent);
				}
				return false;
			}
		});

		final Button helpBtn = (Button) findViewById(R.id.help_btn);
		helpBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					helpBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					helpBtn.setBackgroundResource(R.drawable.green);
					Intent intent = new Intent(getApplicationContext(),
							HelpActivity.class);
					startActivity(intent);
				}
				return false;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}
}
