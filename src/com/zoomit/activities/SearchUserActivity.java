package com.zoomit.activities;

import java.util.List;

import com.zoomit.R;
import com.zoomit.common.BaseDefs;
import com.zoomit.common.Game;
import com.zoomit.common.Player;
import com.zoomit.common.Utils;
import com.zoomit.model.GetGamesClbk;
import com.zoomit.model.GetPlayerClbk;
import com.zoomit.model.Model;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;

public class SearchUserActivity extends BaseActivity {
	private Player enemyPlayer = null;
	private String currentUserName = null;
	private String enemyPlayerName = null;
	private boolean isExistsGame = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_search_user);

		final EditText textUsername = (EditText) findViewById(R.id.ed_search_user);

		final Button searchUserBtn = (Button) findViewById(R.id.btn_search_user);
		searchUserBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					searchUserBtn.setBackgroundResource(R.drawable.purple_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					searchUserBtn.setBackgroundResource(R.drawable.purple);
					Model.instance(SearchUserActivity.this).ifUserExists(
							new GetPlayerClbk() {

								@Override
								public void done(List<Player> list) {
									if (list.isEmpty()) {
										Utils.displayAlertDialog(
												SearchUserActivity.this,
												R.string.msg_cannot_find_user);
									} else {
										enemyPlayer = list.get(0);
										startNewGame();
									}
								}
							}, textUsername.getText().toString());
				}
				return false;
			}
		});
	}

	private void startNewGame() {
		currentUserName = Model.instance(this).getCurrentPlayer().getUsername();
		enemyPlayerName = enemyPlayer.getUsername();

		if (currentUserName.equals(enemyPlayerName)) {
			Utils.displayAlertDialog(SearchUserActivity.this,
					R.string.msg_cannot_play);
			return;
		}

		// check if users already playing
		Model.instance(this).getAllGames(new GetGamesClbk() {

			@Override
			public void done(List<Game> list) {
				for (Game game : list) {
					if (game.getPlayer1().equals(currentUserName)
							&& game.getPlayer2().equals(enemyPlayerName)) {
						isExistsGame = true;
						Utils.displayAlertDialog(SearchUserActivity.this,
								R.string.msg_game_exists);
						return;
					} else if (game.getPlayer1().equals(enemyPlayerName)
							&& game.getPlayer2().equals(currentUserName)) {
						isExistsGame = true;
						Utils.displayAlertDialog(SearchUserActivity.this,
								R.string.msg_game_exists);
						return;
					}
				}

				String gameId = Utils.generateUUID().toString().substring(0,8);
				Game game = new Game(gameId, currentUserName, currentUserName,
						enemyPlayerName, 0);
				Model.instance(SearchUserActivity.this).saveToDb(game);

				Intent intent = new Intent(getApplicationContext(),
						SelectImageActivity.class);
				intent.putExtra(BaseDefs.GAME_ID, gameId);
				intent.putExtra(BaseDefs.ENEMY_PLAYER, enemyPlayerName);
				startActivity(intent);

			}
		});
	}
}
