package com.zoomit.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.widget.Button;

import com.zoomit.R;
import com.zoomit.common.Utils;
import com.zoomit.model.GetScoreClbk;
import com.zoomit.model.Model;

public class AppStartActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_app_start);

		final Button playBtn = (Button) findViewById(R.id.play_appstart);
		playBtn.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					playBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					playBtn.setBackgroundResource(R.drawable.green);
					Intent intent = new Intent(getApplicationContext(),
							MainAppActivity.class);
					startActivity(intent);
				}
				return false;
			}
		});

		final Button logoutBtn = (Button) findViewById(R.id.logout_appstart);
		logoutBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					logoutBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					logoutBtn.setBackgroundResource(R.drawable.green);
					logout();
					Intent intent = new Intent(getApplicationContext(),
							MainActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
				}

				return false;
			}
		});

		final Button helpBtn = (Button) findViewById(R.id.btn_help_appstart);
		helpBtn.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					helpBtn.setBackgroundResource(R.drawable.green_down);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					helpBtn.setBackgroundResource(R.drawable.green);
					Intent intent = new Intent(getApplicationContext(),
							HelpActivity.class);
					startActivity(intent);
				}
				return false;
			}
		});
	}

	private void logout() {
		Model.instance(this).logout();
	}
}
