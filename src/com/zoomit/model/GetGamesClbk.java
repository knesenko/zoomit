package com.zoomit.model;

import java.util.List;

import com.zoomit.common.Game;

public interface GetGamesClbk {
	public void done(List<Game> list);
}
