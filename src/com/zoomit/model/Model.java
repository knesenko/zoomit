package com.zoomit.model;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.zoomit.common.BaseDefs;
import com.zoomit.common.Game;
import com.zoomit.common.Player;
import com.zoomit.common.RunningGame;

import android.content.Context;

public class Model {
	private final static Model instance = new Model();
	private static boolean init = false;
	private Player currentPlayer;
	private ParseUser currentUser;

	private Model() {
	}

	public static Model instance() {
		return instance;
	}

	public static Model instance(Context context) {
		if (!init) {
			instance.init(context);
			init = true;
		}
		return instance;
	}

	private void init(Context context) {
		Parse.initialize(context, BaseDefs.APP_ID, BaseDefs.CLIENT_KEY);
	}

	public void uploadFileToDb(byte[] data, String gameId) {
		String pictureName = gameId + ".png";
		ParseFile data_file = new ParseFile(pictureName, data);
		data_file.saveInBackground();

		ParseObject parsePic = new ParseObject(BaseDefs.PICTURE);
		parsePic.put(BaseDefs.PICTURE, data_file);
		parsePic.put(BaseDefs.GAME_ID, gameId);
		parsePic.saveInBackground();
	}

	public void getFileFromDb(String gameId, final GetFileClbk clbk) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(BaseDefs.PICTURE);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				ParseFile file = (ParseFile) list.get(0).get(BaseDefs.PICTURE);

				file.getDataInBackground(new GetDataCallback() {

					@Override
					public void done(byte[] file, ParseException e) {
						clbk.done(file);
					}
				});

			}
		});

	}

	public void ifUserExists(final GetPlayerClbk clbk, String username) {
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo(BaseDefs.USERNAME, username);
		query.findInBackground(new FindCallback<ParseUser>() {

			@Override
			public void done(List<ParseUser> arg0, ParseException arg1) {
				List<Player> list = new ArrayList<Player>();
				for (ParseUser user : arg0) {
					list.add(new Player(user.get(BaseDefs.USERNAME).toString()));
				}
				clbk.done(list);
			}
		});
	}

	public void updateCurrentPlayerUser() {
		ParseUser parseUser = ParseUser.getCurrentUser();
		currentUser = parseUser;
		if (parseUser != null) {
			Player player = new Player(currentUser.get(BaseDefs.USERNAME)
					.toString());
			currentPlayer = player;
		}

	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public boolean isCurrentUserLoggedIn() {
		if (this.currentUser == null) {
			return false;
		}

		return true;
	}

	@SuppressWarnings("static-access")
	public void logout() {
		if (currentUser != null) {
			currentUser.logOut();
			currentPlayer = null;
		}
	}

	public void saveToDb(Game game) {
		ParseObject parseGame = new ParseObject(BaseDefs.GAME);
		parseGame.put(BaseDefs.GAME_ID, game.getId());
		parseGame.put(BaseDefs.INIT, game.getInit());
		parseGame.put(BaseDefs.PLAYER_1, game.getPlayer1());
		parseGame.put(BaseDefs.PLAYER_2, game.getPlayer2());
		parseGame.put(BaseDefs.LEVEL, game.getLevel());

		parseGame.saveInBackground();
	}

	public void saveToDb(final RunningGame game) {
		final String gameId = game.getGameId();

		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(BaseDefs.RUNNING_GAME);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> runningGames, ParseException e) {
				if (!runningGames.isEmpty()) {
					runningGames.get(0).put(BaseDefs.GAME_ID, gameId);
					runningGames.get(0).put(BaseDefs.SENDER, game.getSender());
					runningGames.get(0).put(BaseDefs.ANSWER, game.getAnswer());
					runningGames.get(0).put(BaseDefs.QUESTION_TYPE,
							game.getQuestionType());
					runningGames.get(0).put(BaseDefs.START_POINT,
							game.getStartPoint());
					runningGames.get(0).put(BaseDefs.TURN, game.getTurn());
					runningGames.get(0).saveInBackground();
				} else {
					// create new running game
					ParseObject parseGame = new ParseObject(
							BaseDefs.RUNNING_GAME);
					parseGame.put(BaseDefs.GAME_ID, game.getGameId());
					parseGame.put(BaseDefs.SENDER, game.getSender());
					parseGame.put(BaseDefs.ANSWER, game.getAnswer());
					parseGame.put(BaseDefs.QUESTION_TYPE,
							game.getQuestionType());
					parseGame.put(BaseDefs.START_POINT, game.getStartPoint());
					parseGame.put(BaseDefs.TURN, game.getTurn());
					parseGame.saveInBackground();
				}
			}
		});
	}

	public void getAllGames(final GetGamesClbk clbk) {
		ParseQuery<ParseObject> pfq = new ParseQuery<ParseObject>(BaseDefs.GAME);
		pfq.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> games, ParseException e) {
				List<Game> slist = new ArrayList<Game>();
				for (ParseObject g : games) {
					Game game = new Game(g.getString(BaseDefs.GAME_ID), g
							.getString(BaseDefs.INIT), g
							.getString(BaseDefs.PLAYER_1), g
							.getString(BaseDefs.PLAYER_2), g
							.getInt(BaseDefs.LEVEL));
					slist.add(game);
				}
				clbk.done(slist);
			}
		});
	}

	public void getRunningGame(final GetRunningGameClbk clbk, String gameId) {
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(BaseDefs.RUNNING_GAME);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);
		query.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> game, ParseException e) {
				if (!game.isEmpty()) {
					String gameID = game.get(0).getString(BaseDefs.GAME_ID);
					String sender = game.get(0).getString(BaseDefs.SENDER);
					String answer = game.get(0).getString(BaseDefs.ANSWER);
					String point = game.get(0).getString(BaseDefs.START_POINT);
					String turn = game.get(0).getString(BaseDefs.TURN);
					int didGuess = game.get(0).getInt(BaseDefs.DID_GUESS);

					int questionType = game.get(0).getInt(
							BaseDefs.QUESTION_TYPE);
					RunningGame runningGame = new RunningGame(gameID,
							questionType, point, answer, sender, turn, didGuess);
					clbk.done(runningGame);
				}
			}
		});
	}

	public void incrementLevel(String gameId) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(BaseDefs.GAME);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> game, ParseException arg1) {
				game.get(0).increment(BaseDefs.LEVEL);
				game.get(0).saveInBackground();
			}
		});
	}

	public void updateRunningGame(String gameId, final String turn,
			final int didGuess) {
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(BaseDefs.RUNNING_GAME);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> runningGame, ParseException arg1) {
				runningGame.get(0).put(BaseDefs.TURN, turn);
				runningGame.get(0).put(BaseDefs.DID_GUESS, didGuess);
				runningGame.get(0).saveInBackground();
			}
		});
	}

	public void removeGame(String gameId) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(BaseDefs.GAME);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> g, ParseException e) {
				g.get(0).deleteInBackground();
			}
		});

	}

	public void removeRunningGame(final String gameId) {
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(BaseDefs.RUNNING_GAME);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> game, ParseException e) {
				game.get(0).deleteInBackground();
			}
		});

	}

	public void removeFileFromDb(String gameId) {
		ParseQuery<ParseObject> query = ParseQuery.getQuery(BaseDefs.PICTURE);
		query.whereEqualTo(BaseDefs.GAME_ID, gameId);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> pic, ParseException e) {
				pic.get(0).deleteInBackground();;
			}
		});
	}

	public void login(String username, String password, final IsloginClbk clbk) {
		ParseUser.logInInBackground(username, password, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException e) {
				boolean isLoginFinished = false;
				String msg;

				if (user != null) {
					isLoginFinished = true;
					updateCurrentPlayerUser();
					clbk.done(isLoginFinished, null);
				} else {

					switch (e.getCode()) {
					case ParseException.USERNAME_TAKEN:
						msg = BaseDefs.USERNAME_TAKEN;
						break;
					case ParseException.USERNAME_MISSING:
						msg = BaseDefs.USERNAME_NOT_PROVIDED;
						break;
					case ParseException.PASSWORD_MISSING:
						msg = BaseDefs.PASSWORD_NOT_PROVIDED;
						break;
					case ParseException.OBJECT_NOT_FOUND:
						msg = BaseDefs.INVALID_CREDENTIALS;
						break;
					default:
						msg = e.getLocalizedMessage();
						break;
					}

					clbk.done(isLoginFinished, msg);
				}
			}
		});
	}

	public void register(String username, String password,
			final IsRegisterClbk clbk) {

		ParseUser user = new ParseUser();
		user.setUsername(username);
		user.setPassword(password);
		user.put(BaseDefs.SCORE, 0);

		user.signUpInBackground(new SignUpCallback() {
			@Override
			public void done(ParseException e) {
				boolean isRegisterFinished = false;
				String msg;

				if (e == null) {
					isRegisterFinished = true;
					updateCurrentPlayerUser();
					clbk.done(isRegisterFinished, null);
				} else {
					// Sign up didn't succeed. Look at the ParseException
					// to figure out what went wrong
					switch (e.getCode()) {
					case ParseException.USERNAME_TAKEN:
						msg = BaseDefs.USERNAME_TAKEN;
						break;
					case ParseException.USERNAME_MISSING:
						msg = BaseDefs.USERNAME_NOT_PROVIDED;
						break;
					case ParseException.PASSWORD_MISSING:
						msg = BaseDefs.PASSWORD_NOT_PROVIDED;
						break;
					default:
						msg = e.getLocalizedMessage();
						break;
					}

					clbk.done(isRegisterFinished, msg);
				}
			}
		});
	}

	public void updateScore(String username, final int score) {
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo(BaseDefs.USERNAME, username);

		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> users, ParseException e) {
				if (!users.isEmpty()) {
					users.get(0).put(BaseDefs.SCORE, score);
					users.get(0).saveInBackground();
				}
			}
		});
	}

	public void getScore(String username, final GetScoreClbk clbk) {
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo(BaseDefs.USERNAME, username);

		query.findInBackground(new FindCallback<ParseUser>() {
			public void done(List<ParseUser> users, ParseException e) {
				if (!users.isEmpty()) {
					clbk.done(users.get(0).getInt(BaseDefs.SCORE));
				}
			}
		});
	}
}
