package com.zoomit.common;

public class Game {

	private String gameId;
	private String init;
	private String player1;
	private String player2;
	private int level;

	public Game(String id, String init, String player1, String player2,
			int level) {
		this.gameId = id;
		this.init = init;
		this.player1 = player1;
		this.player2 = player2;
		this.level = level;
	}

	public String getId() {
		return gameId;
	}

	public void setId(String id) {
		this.gameId = id;
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public String getPlayer1() {
		return player1;
	}

	public void setPlayer1(String player1) {
		this.player1 = player1;
	}

	public String getPlayer2() {
		return player2;
	}

	public void setPlayer2(String player2) {
		this.player2 = player2;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
