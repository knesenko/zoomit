package com.zoomit.common;

public class BaseDefs {
	public final static int BUFFER_SIZE = 1024;

	/*
	 * Parse settings
	 */
	public final static String APP_ID = "zs7jaJD17WQfDgzul0EvNhV2YBPiN4C0IoPA3A0H";
	public final static String CLIENT_KEY = "pAdUIcexldmKZv19MLCFOKa4XjkYDoZxAIVFtUWn";

	/*
	 * Class names and column names
	 */

	public final static String GAME = "Game";
	public final static String GAME_ID = "game_id";
	public final static String ID = "id";
	public final static String PLAYER = "Player";
	public final static String PLAYER_1 = "player1";
	public final static String PLAYER_2 = "player2";
	public final static String ENEMY_PLAYER = "enemy_player";
	public final static String LEVEL = "level";
	public final static String INIT = "init";
	public final static String USERNAME = "username";
	public final static String PICTURE = "picture";
	public final static String PICTURE_URL = "picture_url";
	public final static String RUNNING_GAME = "RunningGame";
	public final static String SENDER = "sender";
	public final static String SCORE = "score";
	public final static String ANSWER = "answer";
	public final static String START_POINT = "start_point";
	public final static String QUESTION_TYPE = "question_type";
	public final static String TURN = "turn";
	public final static String DID_GUESS = "did_guess";
	/*
	 * Messages
	 */

	public final static String USERNAME_TAKEN = "Sorry, this username has already been taken.";
	public final static String USERNAME_NOT_PROVIDED = "Sorry, you must supply a username to register.";
	public final static String PASSWORD_NOT_PROVIDED = "Sorry, you must supply a password to register.";
	public final static String INVALID_CREDENTIALS = "Sorry, those credentials were invalid.";
	public final static String WAIT_FOR_OTHER_PLAYER = "Wait for other player to finish his turn";

	/*
	 * Others
	 */

	public final static String CLICK_POS = "click_pos";

}
