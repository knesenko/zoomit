package com.zoomit.common;

public class RunningGame {
	private String gameId;
	private int questionType;
	private String startPoint;
	private String answer;
	private String sender;
	private String turn;
	private int didGuess;

	public RunningGame(String gameId, int questionType, String startPoint,
			String answer, String sender, String turn, int didGuess) {
		this.gameId = gameId;
		this.questionType = questionType;
		this.startPoint = startPoint;
		this.answer = answer;
		this.sender = sender;
		this.turn = turn;
		this.didGuess = didGuess;
	}

	public String getGameId() {
		return gameId;
	}

	public String getSender() {
		return sender;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public int getQuestionType() {
		return questionType;
	}

	public void setQuestionType(int questionType) {
		this.questionType = questionType;
	}

	public String getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public int getDidGuess() {
		return didGuess;
	}

	public void setDidGuess(int didGuess) {
		this.didGuess = didGuess;
	}

}
