package com.zoomit.common;

import java.util.List;

import com.zoomit.R;
import com.zoomit.model.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {

	private List<Game> games;
	private Context context;
	private LayoutInflater myInflater;

	public ListAdapter(List<Game> list, Context c) {
		this.games = list;
		this.context = c;
		this.myInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return games.size();
	}

	@Override
	public Game getItem(int position) {
		return games.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = myInflater
					.inflate(R.layout.row_layout, parent, false);
		}

		TextView levelTv = (TextView) convertView
				.findViewById(R.id.level_id_list);
		levelTv.setText(Integer.toString(getItem(position).getLevel()));

		TextView playerTv = (TextView) convertView
				.findViewById(R.id.player_id_list);

		String player2Name = getItem(position).getPlayer2().toString();

		if (player2Name.equals(Model.instance().getCurrentPlayer()
				.getUsername())) {
			player2Name = getItem(position).getPlayer1().toString();
		}
		playerTv.setText(player2Name);

		return convertView;
	}

}
