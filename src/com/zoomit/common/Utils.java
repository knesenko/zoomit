package com.zoomit.common;

import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.zoomit.R;

public class Utils {
	public static String generateUUID() {
		return UUID.randomUUID().toString();
	}

	public static void displayAlertDialog(Activity activity, int msgId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(msgId);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.msg_ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public static int checkStringsMatch(String realAnswer, String usersAnswer) {
		int realScore;

		realAnswer = realAnswer.toLowerCase();
		usersAnswer = usersAnswer.toLowerCase();

		int[] costs = new int[usersAnswer.length() + 1];
		for (int j = 0; j < costs.length; j++)
			costs[j] = j;
		for (int i = 1; i <= realAnswer.length(); i++) {
			// j == 0; nw = lev(i - 1, j)
			costs[0] = i;
			int nw = i - 1;
			for (int j = 1; j <= usersAnswer.length(); j++) {
				int cj = Math
						.min(1 + Math.min(costs[j], costs[j - 1]),
								realAnswer.charAt(i - 1) == usersAnswer
										.charAt(j - 1) ? nw : nw + 1);
				nw = costs[j];
				costs[j] = cj;
			}
		}

		int score = costs[usersAnswer.length()];
		if (score == 0) {
			return 100;
		}

		if (realAnswer.length() <= usersAnswer.length()) {
			realScore = usersAnswer.length() - score;
			return (realScore * 100) / usersAnswer.length();
		} else {
			realScore = realAnswer.length() - score;
			return (realScore * 100) / realAnswer.length();
		}

	}
}