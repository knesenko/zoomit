package com.zoomit.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;

import com.zoomit.R;

public class CustomView extends View {
	Drawable image;
	Rect rect = new Rect(0, 0, 900, 1260);
	public Point pos = new Point();

	public CustomView(Context context) {
		super(context);
		image = getResources().getDrawable(R.drawable.cross);
	}

	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawColor(0x000000);
		image.setBounds(rect);
		image.draw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_MOVE:
			float x = event.getX();
			float y = event.getY();
			
			if(x >= 850 || x <= 40 || y >= 1120 || y <= 20 ){
				break;
			}
			rect.left = (int) x - 600;
			rect.top = (int) y - 820;
			rect.right = (int) x + 600;
			rect.bottom = (int) y + 820;
			pos.x = (int) x;
			pos.y = (int) y;
			break;
		}
		invalidate();
		return true;
	}
}
